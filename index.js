let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];


function register(userName) {

        if (registeredUsers.includes(userName)) {

        alert("Registration failed. Username already exists!");

        } else {

        registeredUsers.push(userName);
        alert("Thank you for registering!");
        }
}

register();
console.log(registeredUsers);


function addFriend(userName) {

        if (registeredUsers.includes(userName)) {

        friendsList.push(userName);
        alert("You have added "+ userName + " as a friend!");

        } else {

        alert("User not found.");
        }

}

addFriend();
console.log(friendsList);


function displayFriendsList() {
    
        if (friendsList.length > 0) {

           friendsList.forEach(function(userName){

                console.log(userName);
        })

        } else {
 
        alert("You currently have" + friendsList.length + "friends. Add one first");

        }       
}


displayFriendsList(friendsList);

function deleteFriend(userName) {

    if (friendsList.length > 0) {
    
        friendsList.pop();
    } else {
        console.log("You have " + friendsList.length +" friend")
    }
}

deleteFriend();

console.log(friendsList);


